const User = require("../models/user.js")
const Course = require("../models/course.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const course = require("../models/course.js");

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
        // 10 -salt

    })

    return newUser.save().then((user,error)=> {
        if(error){
            return false;
        }
        else{
            return true;
        }
    })
}

/*function checkEmailExist{}*/
module.exports.checkEmailExist = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0){
            return true;
        }
        else{
            return false;
        }
    }
    )
}

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null){
            return false;
        }
        else{
            // compareSync is a bcrypt function to compare unhashed password to hashed password
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            //true or false
            if(isPasswordCorrect){
                // Let's give the user a token to a access feature
                return{access: auth.createAccessToken(result)};
            }
            else{
                // If password does not match, else
                return false
            }

        }
    })
}

//// Activity
/*module.exports.getProfile = (req, res) => {
	User.findById(req.body.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}*/


// S38 Activity solution

/*module.exports.getProfile = (reqBody) => {
    return User.findById(reqBody._id).then((result, err)=> {
        if(err){
            return false;
        }
        else{
            result.password = "*****"
            return result;
        }
    })
}*/

// s41
module.exports.getProfile = (request, response) => {

    // will contain your token
    const userData = auth.decode(request.headers.authorization);

    console.log(userData)

    return User.findById(userData.id).then(result => {
        result.password = "******";
        response.send(result);
        
    })
}

// Enroll feature
module.exports.enroll = async (request, response) => {

    const userData = auth.decode(request.headers.authorization)

    let courseName = await Course.findById(request.body.courseId).then(result => result.name);

    let newData = {

        // USER ID AND EMAIL WILL BE RETRIEVED FROM THE REQUEST HEADER( REQUEST HEADER CONTAINS THE USER TOKEN)
        userId: userData.id,
        email: userData.email,
        // courseId WILL BE RETRIEVED FROM THE REQUEST BODY
        courseId: request.body.courseId,
        courseName: courseName
    }

    console.log(newData)

    let isUserUpdated = await User.findById(newData.userId)
    .then(user =>{
        user.enrollments.push({
            courseId: newData.courseId,
            courseName: newData.courseName
        })
        // SAVE THE UPDATED USER INFORMATION FROM THE DATABASE
        return user.save()
        .then(result => {
            console.log(result);
            return true;
        })
        .catch(error => {
            console.log(error);
            return false;
        })
    })
    console.log(isUserUpdated);

    let isCourseUpdated = await Course.findById(newData.courseId)
    .then(course => {

        course.enrollees.push({
            userId: newData.userId,
            email: newData.email
        })
    
    // MINI ACTIVITY -
    // [1]CREATE A CONDITION THAT IF THE SLOTS IS ALREADY ZERO, NO DEDUCTION OF SLOT WILL HAPPEN AND IT 
    // [2] It should have a message in the terminal that the slot is already zero
    // [3]ELSE IF THE SLOT IS NEGATIVE VALUE, IT SHOULD MAKE THE SLOT VALUE TO ZERO

    // Minus the slots available by 1
    // course.slots = course.slots - 1;
        //course.slots -= 1;
        if (course.slots === 0) {
            console.log("Sorry, there are no available slots for enrollment.");
        } 
        
        
        
        return course.save()
        .then(result =>{
            console.log(result);
            return true;
        })
        .catch(error => {
            console.log(error);
            return false;
        })
    
    })

    console.log(isCourseUpdated);

    // Condition will check if the both "user" and "course" document has been updated.
    // TERNARY OPERATOR
    (isUserUpdated == true && isCourseUpdated == true)? response.send(true) : response.send(false);

    // ALTERNATIVE CODE ABOVE
    /* 
        if(isUserUpdated == true && isCourseUpdated == true){
            response.send(true);
        }
        else{
            response.send(false);
        }
    */
}

