const mongoose = require("mongoose");
const course = require("../models/course.js");
const Course = require("../models/course.js");


// Function for adding a course
// 2. update the "addCourse" controller method to implement admin authentication for creating a course.
// NOTE: include screenshot of successful admin addCourse and screenshot of not successful add course by a user that is not admin
module.exports.addCourse = (reqBody, addCourse) => {
	if(addCourse.isAdmin == true){
		let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return newCourse.save().then((newCourse, error) => {
		if(error){
			return error;
		}
		else{
			return true;
		}
	})
	}
	else{
		let message = Promise.resolve("User must be Admin to access this functionality");
		return message.then((value) => {return value});
	}	
	
}





/*module.exports.addCourse = (reqBody) => {

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })

    return newCourse.save().then((newCourse, error) =>{
        if(error){
            return error;
        }
        else{
            return newCourse;
        }
    })
}*/

// GET all course
module.exports.getAllCourse = () => {
    return Course.find({}).then(result => {
        return result;
    })
}

// Get all Active Courses
module.exports.getActiveCourses = () => {
    return Course.find({isActive: true}).then(result => {
        return result;
    })
}

// Get Specific Course
module.exports.getCourse = (courseId) => {
    return Course.findById(courseId).then(result => {
        return result;
    })
}

// [Additional Example]
// [Arrow function to regular function]
// Get ALL course feature converted to regular function
// 'GetSpecificCourseInRegularFunction' acts as a variable/storage of getCourseV2() function so it could be exported
module.exports.GetSpecificCourseInRegularFunction = 
function getCourseV2(courseId){
	return Course.findById(courseId).
	then(
		function sendResult(result){
			return result;
		}
	);
}


// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

///S40

module.exports.archiveCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				
				isActive: newData.isActive
				
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}