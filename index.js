

// dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js")
// to create a express server/application

const app = express("./routes/userRoutes.js");

// Middlewares - allows to bridge our backend application (server) to our front end

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.ebpzzsg.mongodb.net/courseBooking?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Ferrer-Mongo DB Atlas"));

// To allow cross origin resource sharing
app.use(cors());
// to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes)
app.use("/courses", courseRoutes)

app.listen(process.env.PORT || 4000, () =>
    {console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

