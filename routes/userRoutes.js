const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js")
const auth = require("../auth.js");

router.post("/register", (request, response) => {
    userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/checkEmail", (request, response) => {
    userControllers.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController))
})

router.post("/login", (request, response) => {
    userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})

//router.post("/details", userControllers.getProfile)

// s38 Activity
/*router.post("/details", (request, response) => {
    userControllers.getProfile(request.body).then(resultFromController => response.send(resultFromController))
})*/

router.get("/details", auth.verify, userControllers.getProfile)

router.post("/enroll", auth.verify, userControllers.enroll)


module.exports = router;